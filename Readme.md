# SoundQ

This is a sound cue application designed for use in theaters. I've built it for
my local community theater, to give us the ability to execute sound cues as 
easily as we do light cues.

## Features

* Supports common sound formats (`mp3`, `aac`, `wav`, `ogg`)
* 'Remote control' from a smartphone or iPad.
* Fade, Crossfade, Loop, Start Position, End Position
* Per cue settings
* Build your own cross show SoundFX Library
