//----------------------------------------------------------------------------------------------------------------------
// Main server module for Tome.
//
// @module server.js
//----------------------------------------------------------------------------------------------------------------------

// Config
const config = require('./config');

// Logging
const logging = require('trivial-logging');

logging.init(config);
logging.setRootLogger('soundq');
const logger = logging.loggerFor(module);

//----------------------------------------------------------------------------------------------------------------------

const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

// Routes
// const accountsRoute = require('./server/routes/accounts');
// const newsRoute = require('./server/routes/news');
// const imagesetsRoute = require('./server/routes/imagesets');
const routeUtils = require('./server/routes/utils');

//----------------------------------------------------------------------------------------------------------------------

// Build the express app
const app = express();

// Basic request logging
app.use(routeUtils.requestLogger(logger));

// Basic error logging
app.use(routeUtils.errorLogger(logger));

// Session support
// app.use(cookieParser());
app.use(bodyParser.json());

// Setup static serving
app.use(express.static(path.resolve('./dist')));

// Set up our application routes
// app.use('/account', accountsRoute);
// app.use('/news', newsRoute);
// app.use('/imagesets', imagesetsRoute);

// Serve index.html for any html requests, but 404 everything else.
app.get('*', (request, response) => {
    response.format({
        html: routeUtils.serveIndex,
        json: (request, response) =>
        {
            response.status(404).end();
        }
    })
});

// Start the server
const server = app.listen(config.http.port, () =>
{
    const host = server.address().address;
    const port = server.address().port;

    logger.info('SoundQ Server v%s listening at http://%s:%s', require('./package').version, host, port);
});

//----------------------------------------------------------------------------------------------------------------------
